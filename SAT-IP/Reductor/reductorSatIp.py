import os 

# print (__file__) me sirve para la ejecución en debian por las rutas /xxx/xxx/ **Esperar si funciona como lo codifico**

def read_file(file):
    '''Abre un archivo en formato cnf

    Devuelve el contenido del archivo que se lee.
    Se hace uso de la libreria 'os' para obtener las rutas.

    Parámetros:
    file --  ruta para acceder al archivo desde el directorio actual (reductorSatIp.py)

    '''
    # obtengo la ruta de este archivo (reductorSatIp.py)
    current_path = os.path.dirname(os.path.realpath(__file__))
    #construyo la ruta donde esta el archivo que quiero abrir
    file_path =  os.path.join(current_path, file)
    try:
        # abro el archivo en un modo específico
        sat_file = open(file_path, 'r')
        #leo el archivo linea a linea y lo guardo, me devuelve una lista cada posición es una linea del archivo
        content = sat_file.readlines() 
        #cierro archivo abierto en la memoria
        sat_file.close()
        return content

    except:
        sat_file = ''
        print('Error en la lectura del archivo (archivo no encontrado).')
    finally:
        # si el archivo no está cerrado se cierra
        if not (sat_file.closed):
            sat_file.close()   


    

#archivo_sat = open(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..\InstanciasSAT\prueba.cnf'),'r')
#print(archivo_sat.read())
#archivo_sat.close()

def built_file(sat_file):
    '''Transforma un archivo sat a IP con extensión .mzn

    construye un archivo .mzn donde se transforma el problema sat a ip 
    y lo guarda en la carpeta InstanciasMiniZinc
  
    Se hace uso de la libreria 'os' para obtener las rutas.

    Parámetros:
    sat_file --  contenido de un archivo SAT. Es una lista de los renglones

    '''
    mzn_file = ''

    cant_var = 0
    cant_clausulas = 0
    variables = []

    for linea in sat_file:
        if linea[0] == 'c':
             mzn_file = mzn_file + '%'+ linea[1:len(linea)]

        elif linea[0] == 'p':
            mzn_file = mzn_file + "% VARIABLES \n"
            cant_var = int(linea.split()[2])
            cant_clausulas = int(linea.split()[3])
            
            #construyo la lista de variables
            for num_var in range(cant_var):
                variables.append('var'+ str(num_var+1))
                #agrego al archivo la declaración de variables 
                mzn_file = mzn_file + "var bool: "+ "var"+ str(num_var+1) + "\n"
                
            # ____________________RESTRICCION VAR POSITIVAS _____________________________
            mzn_file = mzn_file + "% --------------- Restricción POSITIVAS:  0 <= varx <= 1 --------------- \n"
            for var in variables:
                mzn_file = mzn_file + "constraint "+ "(0 <= "+ var +") /\ ("+ var +" <= 1) \n"
            
            # ____________________RESTRICCION VAR NEGATIVAS _____________________________
            mzn_file = mzn_file + "% --------------- Restricción NEGATIVAS:  0 <=  not varx <= 1 --------------- \n"
            for var in variables:
                mzn_file = mzn_file + "constraint "+ "(0 <= not "+ var +") /\ (not "+ var +" <= 1) \n"
            
            # ____________________RESTRICCION MUTUAMENTE EXCLUYENTES _____________________________
            mzn_file = mzn_file + "% --------------- Restricción MUTUAMENTE EXCLUYENTES:  0 <= varx + not varx <= 1 --------------- \n"
            for var in variables:
                mzn_file = mzn_file + "constraint "+ "(1 <= ("+ var +" + not "+ var +")) /\ (("+ var +" + not "+ var +") <= 1) \n"
            print(mzn_file)


    

            

        #transformar(linea)
        #print(linea)
    
        
 

#EJECUCIÓN
print(built_file(read_file('..\InstanciasSAT\prueba.cnf')))
