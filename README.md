Numeral 1 proyecto de la asignatura Complejidad y Optimización.

Reducción SAT a Programación Entera (IP)
En esta primera parte usted construiá un programa que reciba como entrada una instancia
SAT en formato DIMACS y genere un programa equivalente de Programaci´on Entera (IP) para
MiniZinc. 

Desarrollado en Python 3.7